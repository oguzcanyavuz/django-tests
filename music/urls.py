from django.conf.urls import url
from . import views

app_name = 'music'

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name="detail"),
    url(r'collection/add/$', views.CollectionCreate.as_view(), name='collection-add'),
]
