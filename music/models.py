from django.db import models
from django.core.urlresolvers import reverse


class Collection(models.Model):
    artist = models.CharField(max_length=100)
    title = models.CharField(max_length=250)
    genre = models.CharField(max_length=100)
    album_logo = models.CharField(max_length=1000)

    def get_absolute_url(self):
        return reverse('music:detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title + ", " + self.artist


class Song(models.Model):
    # CASCADE: if album of a particular song is deleted, then the song will also be deleted
    album = models.ForeignKey(Collection, on_delete=models.CASCADE)
    file_type = models.CharField(max_length=10)
    title = models.CharField(max_length=250)
    is_favorite = models.BooleanField(default=False)

    def __str__(self):
        return self.title
