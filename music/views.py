from django.views import generic
from music.models import Collection
from django.views.generic.edit import CreateView, UpdateView, DeleteView


class IndexView(generic.ListView):
    template_name = 'music/index.html'
    context_object_name = 'all_collections'

    def get_queryset(self):
        return Collection.objects.all()


class DetailView(generic.DetailView):
    model = Collection
    template_name = 'music/detail.html'


class CollectionCreate(CreateView):
    model = Collection
    fields = ['artist', 'title', 'genre', 'album_logo']
