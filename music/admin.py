from django.contrib import admin
from .models import Collection, Song

admin.site.register(Collection)
admin.site.register(Song)

